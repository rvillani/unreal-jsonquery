// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "NativeUtils.generated.h"

/**
 * Utility functions that can't be done in Blueprint
 */
UCLASS()
class JSONQUERYDEV_API UNativeUtils : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	/**
	* Returns the absolute path for the requested save slot file.
	*
	* @param SlotName	The name of the save slot.
	*/
	UFUNCTION(BlueprintPure, meta = (DisplayName="Get Save Slot Path"), Category= "Native")
	static FString GetSaveSlotFilePath(const FString& SlotName);
};
