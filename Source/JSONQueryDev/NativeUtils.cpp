// Fill out your copyright notice in the Description page of Project Settings.

#include "JSONQueryDev.h"
#include "NativeUtils.h"

FString UNativeUtils::GetSaveSlotFilePath(const FString& SlotName)
{
	return FString::Printf(TEXT("%sSaveGames/%s.sav"), *FPaths::GameSavedDir(), *SlotName);
}


